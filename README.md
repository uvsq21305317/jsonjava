# JSONjava

Ce programme à pour but la conversion de fichiers depuis et vers les formats JSON et CSV.

Ce projet a été réalisé par Raphael Duprat et Quentin Buot dans le cadre de l'UE *"Compléments de programmation"* du semestre 1 de master informatique de l'UVSQ par Raphaël DUPRAT et Quentin BUOT

***

## Utilisation

L'utilisation du programme est la suivante : 

java nomdujar fichier_à_convertir fichier_config[optionnel]

La prise en charge du fichier de configuration n'est assurée que pour la conversion JSON vers CSV.
La conversion est par défault au format 1:1 si aucun fichier de configuration n'est spécifié.

***

## Fichier de configuration

Le fichier de configuration permet le renommage de colonnes ainsi que des opérations sur leurs valeurs

La syntaxe du fichier de configuration est la suivante : 
#
	#
		"Nouvelle colonne 1"
		"Colonne 1 Opérateur Colonne 2"
		"Nouvelle colonne 2"
		"Colonne 3 Opérateur Colonne 3"	
		.
		.
		.
	#
#

***

## Opérations supportées

Le résultat des opérations arithmétique est sous forme entière, les valeurs flottantes seront représentées par leur partie entière.

Les opérations supportées par le programme sont : 

* abs: valeur absolue
* acos: arc cosinus
* asin: arc sinus
* atan: arc tangente
* cbrt: racine cubique
* ceil: arrondit supérieur
* cos: cosinus
* cosh: cosinus hyperbolique
* exp: exponentielle
* floor: arrondit inférieur
* log: logarithme (base e)
* log2: logarithme (base 2)
* log10: logarithme (base 10)
* sin: sinus
* sinh: sinus hyperbolique
* sqrt: racine carrée
* tan: tangente
* tanh: tangente hyperbolique
* signum: signe de la valeur
* Addition: '2 + 2'
* Subtraction: '2 - 2'
* Multiplication: '2 * 2'
* Division: '2 / 2'

La syntaxe des opérations est : opérateur( attribut ) pour les fonctions, les espaces dans la parenthèse sont **obligatoires** .

Il est également possible de concaténer des chaines de caractères grâce à l'opérateur "+".

Si la variable d'une des règles de configuration ne peut être interprétée comme un nombre, elle sera interprétée comme une chaine de caractères.
Si une clé utilisée pour une opération, le message "ERREUR la valeur de la clé "var" est introuvable" sera écrit comme valeur de la clé.