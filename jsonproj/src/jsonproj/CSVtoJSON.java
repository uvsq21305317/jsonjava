package jsonproj;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONTokener;

import com.github.wnameless.json.unflattener.JsonUnflattener;

/**
 * Cette classe contient une méthode permettant de convertir un fichier CSV en fichier JSON
 * 
 * @author Raphael Duprat, Quentin Buot
 * @version 1.0
 *
 */

public class CSVtoJSON {

	/**
	 * Cette méthode récupère un fichier JSON et créé un fichier "nom du fichier" + "new" + ".json" contenant le fichier converti au format JSON
	 * 
	 * @param args	le fichier CSV à convertir
	 */
	public static void convert(String args)
	{
		PrintWriter convert = null;
		String csv_str = "";
		String json_str = "";
		try 
		{
			convert = new PrintWriter(args.substring(0, args.length()-4) + "new" + ".json");
		} 
		catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
		}
		
		
		try 
		{
			csv_str = new String(Files.readAllBytes(Paths.get(args)));
		} 
		catch (IOException e) 
		{
			System.out.println("Erreur : fichier " + args + " introuvable");
			return;
		} 
		
		//Convertion de la chaine csv en chaine en format JSON
		JSONTokener token = new JSONTokener(csv_str);
		JSONArray array = CDL.toJSONArray(token);
		json_str = array.toString();
		
		//Imbriquation du JSON
		json_str = JsonUnflattener.unflatten(json_str);
		
		convert.println(json_str);
		convert.close();
	}
	

	
}
