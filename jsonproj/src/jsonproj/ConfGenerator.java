package jsonproj;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Génère un fichier configuration
 * 
 * @author Raphael Duprat, Quentin Buot
 * @version 1.0
 *
 */

public class ConfGenerator {

	/**
	 * Cette méthode génère un fichier "configuration" sans règles.
	 * Le fichier sera créé dans le dossier jsonproj.
	 */
	public static void generateDefault()
	{
		//Genere une configuration de base
		PrintWriter writer = null;
		String str = "";
		
		try 
		{
			 writer = new PrintWriter(System.getProperty("user.dir") + System.getProperty("file.separator") + "configuration");
		} 
		catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
		}
		str = "##\n\n##";
		
		writer.println(str);
		writer.close();
	}
	
	
}
