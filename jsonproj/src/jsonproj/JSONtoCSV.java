package jsonproj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.github.wnameless.json.flattener.JsonFlattener;
import net.objecthunter.exp4j.*;

import org.json.CDL;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.io.FileNotFoundException;

/**
 * Cette classe contient les méthodes permettant de convertir un fichier JSON au format CSV.
 * 
 * @author Raphael Duprat, Quentin Buot
 * @version 1.0
 *
 */

public class JSONtoCSV {

	/**
	 * Evalue l'expression expr.
	 * expr peut contenir des clés contenues dans "jobj". La valeur contenue dans ces clés sera utilisée pour réécrire l'expression.
	 * Cette expression est ensuite évaluée grâce à la bibliothèque exp4j
	 * 
	 * @param jobj	Un JSONObject
	 * @param key	Une nouvelle clé associée à la valeur calculée
	 * @param expr	L'expression à calculer
	 * @return Le résultat de l'évaluation de "expr"
	 */
	public static String my_evaluate(JSONObject jobj, String key, String expr)
	{
		String result2 = "NO_VALUE_ERROR";
		Integer result;
		
		String result_str = "";
		
		Expression e;
		String tmp_expr = expr;
		
		//écriture de l'expression
		tmp_expr = tmp_expr.replace('+', ' ');
		tmp_expr = tmp_expr.replace('-', ' ');
		tmp_expr = tmp_expr.replace('/', ' ');
		tmp_expr = tmp_expr.replace('*', ' ');
		tmp_expr = tmp_expr.replace('"', ' ');
		String[] vars = tmp_expr.split(" ");
		
		//ON remplace toutes les variable de la chaine par leur valeur dans le JSONObject
		for(String var : vars)
		{
			if(!var.isEmpty())
			{

				try 
				{
					expr = expr.replaceFirst(var, jobj.get(var).toString());
				} 
				catch (JSONException e1) 
				{
					//Si on repère une constante, ne pas faire l'opération
					//mais on peut continuer d'évaluer sans souci
				}
				
			}
		}
		
		//evaluation de l'expression ainsi créé. Si "illegal argument" c'est qu'une des variables n'est pas
		//évaluable en tant qu'entier, donc on considère que tout est un String et on concatène car c'est la
		//seule opération autorisée sur les strings.
		try 
		{
			e = new ExpressionBuilder(expr.replaceAll("\"", " ")).build();
		} 
		catch (IllegalArgumentException e1) 
		{
			for(String var : vars)
			{
				if(!var.isEmpty())
				{
					try 
					{
						result_str += jobj.get(var).toString();
					} 
					catch (JSONException e2) 
					{
						return ("ERREUR la valeur de la clé (" + var + ") est introuvable");
					}
				}
			}
			return result_str;
		}
		
		result = (((Double)e.evaluate()).intValue());
		result2 = result.toString();
		return result2;
		
	}
	
	/**
	 * Convertit un fichier JSON en fichier CSV en suivant des règles de configurations éventuelles.
	 * 
	 * @param fichier	Le fichier à convertir.
	 * @param config	Les règles de conversion.
	 */
	public static void convert(String fichier , String config) {
		
		String json_str = null;
		String csv_str = "";
		PrintWriter convert = null;
		int i = 0;
		List<String> conf = null;
		
		//Lecture du fichier configuration
		try 
		{
			conf = Files.readAllLines(Paths.get(config), Charset.defaultCharset());
		} 
		catch (IOException e3) 
		{
			e3.printStackTrace();
		}
		
		//créé le fichier destination en csv
		try 
		{
			convert = new PrintWriter(fichier.substring(0, fichier.length()-5) + ".csv");
		} 
		catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
		}
		
		//récupère le contenu fichier JSON dans un string
		try 
		{
			json_str = new String(Files.readAllBytes(Paths.get(fichier)));
		} 
		catch (IOException e) 
		{
			System.out.println("Erreur : fichier " + fichier + " introuvable");
			return;
		} 
		
		//Mise à plat du fichier passé en paramètre
		json_str = JsonFlattener.flatten(json_str);
		
		//Rajoute le nom des attributs de conf dont il faut ensuite calculer les valeurs
		for(i = 0; i< conf.size(); i++)
		{
			json_str = json_str.replace('}', ',');
			if(conf.get(i).equals("#"))
			{
				for(i = i+1; i< conf.size(); i+=2)
				{
					if(conf.get(i).equals("#"))
					{
						csv_str += "\n";
						break;
					}
					json_str += conf.get(i);
					json_str += ":";
					json_str += my_evaluate(new JSONObject(json_str + "\"TMPVALUE\",}"), conf.get(i), conf.get(i+1));
					json_str += ",";
				}
			}
		}
		json_str += "}";
		
		//Récupération des clés dans array et écriture dans le fichier
		JSONObject obj = new JSONObject(json_str);	
		java.util.Set<java.lang.String> keys = obj.keySet();
		Object[] akeys = keys.toArray();
		JSONArray keysArray = new JSONArray(akeys);
		csv_str = CDL.rowToString(keysArray);
		
		//JSONObject contenant le fichier json passé en paramètre
		JSONObject jsonobj = new JSONObject(new JSONTokener(json_str));
		
		//Boucle qui lit une � une les valeurs correspondantes aux clés et les écrit.
		//Si ni Integer ni String, on considère que la valeur est nulle et remplacée par un String "null"
		for(i = 0; i<keys.size(); i++)
		{
			Object content = jsonobj.get((String)akeys[i]);
			try 
			{
				int number = (Integer)content;
				csv_str =  csv_str + Integer.toString(number) + ",";
			} 
			catch (ClassCastException e) 
			{
				try 
				{
					String str = (String)content;
					csv_str = csv_str + str + ",";
				} 
				catch (ClassCastException e2) 
				{
					csv_str = csv_str + "null" + ",";
				}
			}
		}
		//écriture dans le fichier
		convert.println(csv_str);
		//Fermeture du fichier
		convert.close();
		
	}

}
