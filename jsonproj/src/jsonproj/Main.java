package jsonproj;

import java.lang.String;

/**
 * Classe principale permettant d'executer le programme.
 * 
 * @author Raphael Duprat, Quentin Buot
 * @version 1.0
 * 
 */

public class Main {
	
	/**
	 * Cette méthode récupère un ou deux fichiers en entrée et convertit un fichier JSON en fichier CSV, ou un fichier CSV en fichier JSON.
	 * Il est également possible de rajouter en paramètre un fichier configuration décrivant des règles qui seront appliquées lors de la conversion JSON to CSV
	 * Le format du fichier configuration est décrit dans le fichier README.
	 * 
	 * @param args	[0] : Les chemins absolu du fichier .json ou .csv, [1] : le chemin absolu du fichier configuration.
	 */
	
	public static void main(String[] args)
	{
		
		String fichier = "";
		String config = "";
		
		System.out.println(args[0] + " \n" );
		switch(args.length)
		{
			case 0 :
				System.out.println("Aucun fichier specifie \n" );
				return;
				
			case 1 :
				
				if( !(args[0].endsWith(".json")) && !(args[0].endsWith(".csv")) )
				{
						System.out.println("Le fichier specifie n'est pas au format JSON ou CSV \n" );
						return;
				}
				System.out.println("Aucun fichier de configuration specifie, creation d'une configuration par default \n" );
				ConfGenerator.generateDefault();
				fichier = args[0];
				config = System.getProperty("user.dir") + System.getProperty("file.separator") + "configuration";
				break;
			
			default :
					fichier = args[0];
					config = args[1];
				
		}
		
		if( args[0].endsWith(".json"))
		{
			System.out.println("Conversion de votre fichier JSON vers CSV \n" );
			JSONtoCSV.convert(fichier,config);
			System.out.println("Conversion terminee\n" );
		}
		else if( args[0].endsWith(".csv")) 
		{
			System.out.println("Conversion de votre fichier CSV vers JSON \n" );
			CSVtoJSON.convert(fichier);
			System.out.println("Conversion terminee \n" );
		}
		else
		{
			System.out.println("Le fichier specifie n'est pas au format JSON ou CSV \n" );
		}
		
	}
}
