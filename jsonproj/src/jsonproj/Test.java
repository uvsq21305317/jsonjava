package jsonproj;



import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

/**
 * Classe et méthodes de test
 * 
 * @author Raphael Duprat, Quentin Buot
 * @version 1.0
 * 
 */

public class Test {

	/**
	 * Teste une expression avec priorité, tous les opérateurs et une fonction
	 */
	@org.junit.Test
	public void testExpr() {
		String result = JSONtoCSV.my_evaluate(new JSONObject().put("JSON", "Hello, World!"), "JSON", "3+2*5 + (cos(100)*1000)");
		assertEquals("875", result);
	}
	
	/**
	 * Test une conversion de JSON à CSV
	 */
	@org.junit.Test
	public void testJSONtoCSV() 
	{
		//String path = "/home/user/Bureau/jsonjava/testfiles";
		String path = System.getProperty("user.dir");
		path = path.substring(0, path.length()-9);
		path += "/testfiles";
		File file1 = new File(path + "/testfile3.csv");
		File file2 = new File(path + "/testfile3expected.csv");
		JSONtoCSV.convert(path + "/testfile3.json", path + "/configuration3");

		try {
			assertTrue("fichiers différents", FileUtils.contentEquals(file1, file2));
		} catch (IOException e) 
		{
			fail("IOException");
		}
	}
	
	/**
	 * Teste une conversion de CSV à JSON
	 */
	@org.junit.Test
	public void testCSVtoJSON() 
	{
		String path = System.getProperty("user.dir");
		path = path.substring(0, path.length()-9);
		path += "/testfiles";
		File file1 = new File(path + "/testfile3new.json");
		File file2 = new File(path + "/testfile3expected.json");
		CSVtoJSON.convert(path + "/testfile3.csv");

		try {
			assertTrue("fichiers différents", FileUtils.contentEquals(file1, file2));
		} catch (IOException e) 
		{
			fail("IOException");
		}
	}
}
